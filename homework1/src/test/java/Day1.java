import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

import io.restassured.response.Response;

import java.util.List;

public class Day1 {
    private static final String BASE_URL = "https://booking-api-dev.herokuapp.com";
    private static final String USERNAME = "admin";
    private static final String PASSWORD = "password123";

    private static String authToken;
    private static int bookingId;

    @BeforeAll
    public static void setup() {
        RestAssured.baseURI = BASE_URL;
         authToken = given()
                 .contentType(ContentType.JSON)
                 .body("{\"username\": \"" + USERNAME + "\", \"password\" : \"" + PASSWORD + "\"}")
                 .when()
                 .post("/auth")
                 .then()
                 .statusCode(200)
                 .body("token", not(isEmptyOrNullString()))
                 .body("$", hasKey("token"))
                 .extract()
                 .path("token");
        System.out.println("Token received: " + authToken);

        String requestBody = "{\n" +
                "    \"firstname\" : \"John\",\n" +
                "    \"lastname\" : \"Doe\",\n" +
                "    \"totalprice\" : 200,\n" +
                "    \"depositpaid\" : true,\n" +
                "    \"bookingdates\" : {\n" +
                "        \"checkin\" : \"2024-02-10\",\n" +
                "        \"checkout\" : \"2024-02-15\"\n" +
                "    },\n" +
                "    \"additionalneeds\" : \"Breakfast\"\n" +
                "}";

        bookingId = given()
                .contentType(ContentType.JSON)
                .header("Authorization", "Bearer" + authToken)
                .body(requestBody)
                .when()
                .post("/booking")
                .then()
                .statusCode(200)
                .extract()
                .path("bookingid");
        System.out.println("New booking created with ID: " + bookingId + requestBody);
    }
    @Test
    public void authenticationTest() {

        authToken = given()
                .contentType(ContentType.JSON)
                .body("{\"username\": \"" + USERNAME + "\", \"password\" : \"" + PASSWORD + "\"}")
                .when()
                .post("/auth")
                .then()
                .statusCode(200)
                .body("token", not(isEmptyOrNullString()))
                .body("$", hasKey("token"))
                .extract()
                .path("token");


        assertNotNull(authToken);
        System.out.println("Token received: " + authToken);
    }


    @Test
    public void createBookingTest() {
        RestAssured.baseURI = "https://booking-api-dev.herokuapp.com";
        RestAssured.port = 443;

        String requestBody = "{\n" +
                "    \"firstname\" : \"Sunny2\",\n" +
                "    \"lastname\" : \"Brown\",\n" +
                "    \"totalprice\" : 115,\n" +
                "    \"depositpaid\" : true,\n" +
                "    \"bookingdates\" : {\n" +
                "        \"checkin\" : \"2024-03-01\",\n" +
                "        \"checkout\" : \"2024-03-05\"\n" +
                "    },\n" +
                "    \"additionalneeds\" : \"Breakfast\"\n" +
                "}";
        String response =
                given()
                        .header("Content-Type", ContentType.JSON)
                        .body(requestBody)
                        .when()
                        .post("/booking")
                        .then()
                        .statusCode(200)
                        .body("bookingid", notNullValue())
                        .body("booking.firstname", equalTo("Sunny2"))
                        .body("booking.lastname", equalTo("Brown"))
                        .body("booking.totalprice", equalTo(115))
                        .body("booking.depositpaid", equalTo(true))
                        .body("booking.bookingdates.checkin", equalTo("2024-03-01"))
                        .body("booking.bookingdates.checkout", equalTo("2024-03-05"))
                        .body("booking.additionalneeds", equalTo("Breakfast"))
                        .extract().asString();
        System.out.println("Response: " + response);


    }

    @Test
    public void updateAndDeleteBookingTest() {
        RestAssured.port = 443;

        String updatedBookingData = "{\n" +
                " \"firstname\" : \"James\",\n" +
                " \"lastname\" : \"Black\",\n" +
                " \"totalprice\" : 125,\n" +
                " \"depositpaid\" : true,\n" +
                " \"bookingdates\" : {\n" +
                " \"checkin\" : \"2024-02-23\",\n" +
                " \"checkout\" : \"2024-02-28\"\n" +
                " },\n" +
                " \"additionalneeds\" : \"Breakfast\"\n" +
                "}";
        given()
                .contentType(ContentType.JSON)
                .headers("Content-Type", ContentType.JSON.toString(), "Accept", ContentType.JSON.toString(), "Cookie", "token=" +authToken)
                .body(updatedBookingData)
                .when()
                .put("/booking/" +bookingId)
                .then()
                .statusCode(200)
                .log().all();

        given()
                .headers("Content-Type", ContentType.JSON.toString(), "Accept", ContentType.JSON.toString(), "Cookie", "token=" +authToken)
                .when()
                .delete("/booking/{bookingId}", bookingId)
                .then()
                .statusCode(201);
    }

}
